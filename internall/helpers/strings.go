package helpers

import "regexp"

var nonAlphanumericRegexWithoutDot = regexp.MustCompile(`[^\d\.]`)

// ClearStringToDigit remove all non digit characters (only numbers and dots)
func ClearStringToDigit(str string) string {
	return nonAlphanumericRegexWithoutDot.ReplaceAllString(str, "")
}
