package helpers

import "testing"

func TestClearStringToDigit(t *testing.T) {
	cases := []struct {
		given    string
		expected string
	}{
		{},
		{
			given:    "1.1.1",
			expected: "1.1.1",
		},
		{
			given:    "go v1.1.1",
			expected: "1.1.1",
		},
		{
			given:    "go v1.1.1c",
			expected: "1.1.1",
		},
		{
			given:    ",1.1.1:latest",
			expected: "1.1.1",
		},
	}

	for _, tc := range cases {
		if res := ClearStringToDigit(tc.given); res != tc.expected {
			t.Errorf("For given: %s expected: %s bot got: %s", tc.given, tc.expected, res)
		}
	}
}
