package version

import (
	"fmt"
	"gitlab.com/SzymonSkurski/go-auto-updater/internall/helpers"
	"strconv"
	"strings"
)

type Tag struct {
	Version int
	Major   int
	Minor   int
}

type Tags []*Tag

func (tags Tags) ToStrings(prefix, suffix string) []string {
	str := make([]string, len(tags))

	for i, tag := range tags {
		str[i] = fmt.Sprintf("%s%s%s", prefix, tag.String(), suffix)
	}

	return str
}

func (tags Tags) GetPosition(pos *Tag) int {
	for i, tag := range tags {
		if tag.IsSame(pos) {
			return i
		}
	}

	return 0
}

func New(v, mj, mi int) *Tag {
	tag := &Tag{}

	tag.Reset(v, mj, mi)

	return tag
}

func (t *Tag) Reset(v, mj, mi int) {
	t.Version = v
	t.Major = mj
	t.Minor = mi
}

func (t *Tag) String() string {
	return fmt.Sprintf("%d.%d.%d", t.Version, t.Major, t.Minor)
}

// IsLatest compare is tag latest than given tag
func (t *Tag) IsLatest(c *Tag) bool {
	if c == nil {
		return true
	}
	if t.Version != c.Version {
		return t.Version > c.Version
	}
	if t.Major != c.Major {
		return t.Major > c.Major
	}

	return t.Minor > c.Minor
}

func (t *Tag) IsSame(cTag *Tag) bool {
	return t.Version == cTag.Version &&
		t.Major == cTag.Major &&
		t.Minor == cTag.Minor
}

func (t *Tag) IsEmpty() bool {
	return t.Version == 0 && t.Major == 0 && t.Minor == 0
}

// SetFromString set tag from string Version.Major.Minor like 1.2.12
func (t *Tag) SetFromString(s string) {
	s = helpers.ClearStringToDigit(s)
	tagParts := strings.Split(s, ".")
	if len(tagParts) < 3 {
		return
	}

	v, _ := strconv.Atoi(tagParts[0])
	major, _ := strconv.Atoi(tagParts[1])
	minor, _ := strconv.Atoi(tagParts[2])

	t.Version = v
	t.Major = major
	t.Minor = minor
}
