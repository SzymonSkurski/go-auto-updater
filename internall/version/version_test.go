package version

import (
	"testing"
)

func TestIsLatest(t *testing.T) {
	cases := []struct {
		givenTag   *Tag
		compareTag *Tag
		expected   bool
	}{
		{
			givenTag:   New(0, 0, 0), // not latest
			compareTag: New(0, 0, 0),
			expected:   false,
		},
		{
			givenTag:   New(0, 0, 21), // not latest
			compareTag: New(0, 0, 37),
			expected:   false,
		},
		{
			givenTag:   New(0, 0, 37), //latest
			compareTag: New(0, 0, 21),
			expected:   true,
		},
		{
			givenTag:   New(0, 0, 22), // not latest
			compareTag: New(0, 1, 21),
			expected:   false,
		},
		{
			givenTag:   New(0, 1, 1), // latest
			compareTag: New(0, 0, 22),
			expected:   true,
		},
		{
			givenTag:   New(0, 21, 37), // not latest
			compareTag: New(1, 1, 21),
			expected:   false,
		},
		{
			givenTag:   New(10, 1, 0), // latest
			compareTag: New(9, 21, 37),
			expected:   true,
		},
	}

	for _, tc := range cases {
		if res := tc.givenTag.IsLatest(tc.compareTag); res != tc.expected {
			t.Errorf("expected that given tag: %s is %s latest than compare tag: %s", tc.givenTag.String(), boolToString(tc.expected), tc.compareTag.String())
		}
	}
}

func TestSetFromString(t *testing.T) {
	cases := []struct {
		given    string
		expected string
	}{
		{
			expected: "0.0.0",
		}, // empty
		{
			given:    "1.1.1",
			expected: "1.1.1",
		},
		{
			given:    "1.1.1.0",
			expected: "1.1.1",
		},
		{
			given:    "go v1.1.1linux",
			expected: "1.1.1",
		},
	}

	for _, tc := range cases {
		tt := Tag{}
		tt.SetFromString(tc.given)
		if res := tt.String(); res != tc.expected {
			f := "Tag for given: %s expected: %s but got: %s"
			t.Errorf(f, tc.given, tc.expected, res)
		}
	}
}

func boolToString(b bool) string {
	if b {
		return ""
	}

	return "not"
}
