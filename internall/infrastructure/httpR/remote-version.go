package httpR

import (
	"fmt"
	"gitlab.com/SzymonSkurski/go-auto-updater/internall/ui"
	"io"
	"log"
	"net/http"
	"regexp"
	"strings"
	"time"

	"gitlab.com/SzymonSkurski/go-auto-updater/internall/values"
	"gitlab.com/SzymonSkurski/go-auto-updater/internall/version"
)

type GoRemoteVersion struct {
	local     *version.Tag
	latest    *version.Tag
	all       version.Tags
	loaded    bool
	loadOlder bool
}

func NewGoRemoteVersion(local *version.Tag) *GoRemoteVersion {
	return &GoRemoteVersion{
		local: local,
	}
}

func (rv *GoRemoteVersion) GetAllVersions() version.Tags {
	if !rv.loaded {
		rv.load()
	}

	return rv.all
}

func (rv *GoRemoteVersion) GetLatestVersion() *version.Tag {
	if !rv.loaded {
		rv.load()
	}

	return rv.latest
}

func (rv *GoRemoteVersion) GetOlderVersions(tag *version.Tag) version.Tags {
	if !rv.loaded {
		return rv.GetAllVersions()
	}

	rv.loadOlder = true
	rv.local = tag
	rv.load()

	return rv.all
}

func (rv *GoRemoteVersion) load() {
	quitChan := make(chan bool)
	defer func() {
		quitChan <- true
	}()

	ui.New().Msg("getting remote versions").Display()

	go rv.get(quitChan)

	ui.NewWaitingBar(func() {
		fmt.Print(".")
	}, 500*time.Millisecond, quitChan).Run()

	select {
	case <-quitChan:
		rv.loaded = true
		return
	case <-time.After(15 * time.Second):
		{
			ui.New().Warn("cannot download remote version, time out").Display()
			return
		}
	}
}

func (rv *GoRemoteVersion) get(quitChan chan bool) {
	rv.all = []*version.Tag{}
	rv.extractBody(rv.mustGetRemote())
	defer func() {
		quitChan <- true
	}()
}

func (rv *GoRemoteVersion) mustGetRemote() string {
	url := values.GoRepositoryURL
	tag := rv.local
	if rv.loadOlder && tag != nil && tag.Version != 0 {
		url = fmt.Sprintf("%s/?after=go%d.%d.%d", url, tag.Version, tag.Major, tag.Minor)
	}

	resp, err := http.Get(url)

	defer func(resp *http.Response) {
		_ = resp.Body.Close()
	}(resp)

	if err != nil {
		msg := fmt.Sprintf("cannot get go tags from %s, rason: %s", url, err.Error())

		panic(msg)
	}

	if resp.StatusCode != http.StatusOK {
		msg := fmt.Sprintf("response code: %d", resp.StatusCode)

		panic(msg)
	}

	// have 200

	bodyBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	return string(bodyBytes)
}

func (rv *GoRemoteVersion) extractBody(body string) {
	lines := strings.Split(body, "\n")
	tagsM := map[string]*version.Tag{}

	for _, line := range lines {
		if rv.isContainsGoTag(line) {
			lTag := rv.extractGoTag(line)

			tagsM[lTag.String()] = lTag

			if lTag.IsLatest(rv.latest) {
				rv.latest = lTag
			}
		}
	}

	for {
		lTag := version.New(0, 0, 0)
		for _, t := range tagsM {
			if t.IsLatest(lTag) {
				lTag = t
			}
		}

		rv.all = append(rv.all, lTag)

		delete(tagsM, lTag.String())

		if len(tagsM) == 0 {
			break
		}
	}
}

func (rv *GoRemoteVersion) extractGoTag(s string) *version.Tag {
	tag := version.New(0, 0, 0)
	spl := strings.Split(s, `/golang/go/archive/refs/tags/go`)
	if len(spl) < 2 {
		return tag
	}

	tag.SetFromString(spl[1])

	return tag
}

func (rv *GoRemoteVersion) isContainsGoTag(s string) bool {
	pattern := `/golang/go/archive/refs/tags/go\d{1,}\.\d{1,}\.\d{1,}`
	ok, _ := regexp.MatchString(pattern, s)

	return ok
}
