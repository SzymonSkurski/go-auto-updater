package linux

import (
	"fmt"
	"gitlab.com/SzymonSkurski/go-auto-updater/internall/values"
	"os/exec"
	"strings"
)

type DownloadPath struct {
	path string
	dir  string
}

func NewDownloadPath(downloadPath, downloadDirName string) *DownloadPath {
	dp := &DownloadPath{
		dir: downloadDirName,
	}

	dp.mustSetLocalDownloadsPath(downloadPath)

	return dp
}

func (dp *DownloadPath) GetDownloadPath() string {
	return dp.path
}

func (dp *DownloadPath) mustPermitDownloadPath() {
	chmod := fmt.Sprintf("sudo chmod +rwx %s", dp.path)
	cmd := exec.Command(values.Bash, "-c", chmod)
	if _, err := cmd.Output(); err != nil {
		panic(err)
	}
}

func (dp *DownloadPath) mustSetLocalDownloadsPath(dPath string) {
	if dPath != "" {
		dp.path = dPath

		return
	}

	cmd := exec.Command("whoami")

	stdout, err := cmd.Output()
	if err != nil {
		panic("cannot set local Downloads path, check home ~ alias in u system " + err.Error())
	}

	p := strings.Trim(string(stdout), "\n")

	dp.path = fmt.Sprintf("/home/%s/%s", p, dp.dir)
}
