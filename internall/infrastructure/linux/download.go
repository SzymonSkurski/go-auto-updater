package linux

import (
	"fmt"
	"gitlab.com/SzymonSkurski/go-auto-updater/internall/ui"
	"io"
	"net/http"
	"os"
	"time"

	"gitlab.com/SzymonSkurski/go-auto-updater/internall/version"
)

type GoDownload struct {
	downloadPath string
}

func NewGoDownload(downloadPath string) *GoDownload {
	return &GoDownload{
		downloadPath: downloadPath,
	}
}

func (d *GoDownload) Download(v *version.Tag) error {
	filepath := fmt.Sprintf("%s/go%s.linux-amd64.tar.gz", d.downloadPath, v.String())

	// Check if a file exists
	if _, err := os.Stat(filepath); err == nil {
		//fmt.Println("file ", filepath, " already exist")
		return nil
	}

	url := fmt.Sprintf("https://go.dev/dl/go%s.linux-amd64.tar.gz", v.String())

	// Create dir
	_ = os.Mkdir(d.downloadPath, 0777)

	// Create the tar.gz file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	ui.New().Msgf("getting from %s", url).Display()
	respChan := make(chan httpGetResponse, 1)
	dotsChan := make(chan bool, 1)
	resR := httpGetResponse{}

	go getData(url, respChan)

	// Waiting bar
	ui.NewWaitingBar(func() {
		fmt.Print(".")
	}, 500*time.Millisecond, dotsChan).Run()

	defer func(dotsChan chan bool) {
		dotsChan <- true // close dots
	}(dotsChan)

	select {
	case res := <-respChan:
		resR = res
	case <-time.After(time.Second * 30):
		{
			_ = os.Remove(filepath)
			fmt.Println("timeout")

			return fmt.Errorf("%s get timeout", url)
		}
	}

	defer resR.response.Body.Close()

	if resR.err != nil {
		return fmt.Errorf("download error: %s", err.Error())
	}

	// Check server response
	if resR.response.StatusCode != http.StatusOK {
		return fmt.Errorf("bad status: %s", resR.response.Status)
	}

	// Writer the body to file
	_, err = io.Copy(out, resR.response.Body)
	if err != nil {
		return err
	}

	return nil
}

func getData(url string, ch chan httpGetResponse) {
	resp, err := http.Get(url)

	ch <- httpGetResponse{
		err:      err,
		response: resp,
	}
}

type httpGetResponse struct {
	err      error
	response *http.Response
}
