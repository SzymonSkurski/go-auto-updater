package linux

import (
	"fmt"
	"gitlab.com/SzymonSkurski/go-auto-updater/internall/values"
	"os/exec"
)

type GoBackup struct {
	path       string
	backupPath string
}

func NewGoBackup(path, backupPath string) *GoBackup {
	return &GoBackup{
		path:       path,
		backupPath: backupPath,
	}
}

// Store current local go
func (b *GoBackup) Store() error {
	mv := fmt.Sprintf("sudo mv %s %s", b.path, b.backupPath)
	cmd := exec.Command(values.Bash, "-c", mv)
	_, err := cmd.Output()

	return err
}

// Restore go from backup
func (b *GoBackup) Restore() error {
	mv := fmt.Sprintf("sudo mv %s %s", b.backupPath, b.path)
	cmd := exec.Command(values.Bash, "-c", mv)
	_, err := cmd.Output()

	return err
}

func (b *GoBackup) Delete() error {
	rm := fmt.Sprintf("sudo rm -rf %s", b.backupPath)
	cmd := exec.Command("/bin/sh", "-c", rm)
	_, err := cmd.Output()

	return err
}
