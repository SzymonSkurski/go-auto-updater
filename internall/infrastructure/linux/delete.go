package linux

import (
	"fmt"
	"os/exec"

	"gitlab.com/SzymonSkurski/go-auto-updater/internall/values"
)

type GoDelete struct {
	goPath string
}

func NewGoDelete(goPath string) *GoDelete {
	return &GoDelete{
		goPath: goPath,
	}
}

func (gd *GoDelete) DeleteLocal() error {
	//fmt.Println("delete current go version")
	rm := fmt.Sprintf("sudo rm -rf %s", gd.goPath)
	cmd := exec.Command(values.Bash, "-c", rm)
	_, err := cmd.Output()

	if err != nil {
		//fmt.Println("delete current go version: ", err.Error())
		return err
	}

	return nil
}
