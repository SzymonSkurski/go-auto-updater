package linux

import (
	"fmt"
	"gitlab.com/SzymonSkurski/go-auto-updater/internall/values"
	"gitlab.com/SzymonSkurski/go-auto-updater/internall/version"
	"os/exec"
)

type GoInstall struct {
	extractDir   string
	downloadPath string
}

func NewGoInstall(extractDir, downloadPath string) *GoInstall {
	return &GoInstall{
		extractDir:   extractDir,
		downloadPath: downloadPath,
	}
}

func (gi *GoInstall) Install(tag *version.Tag) error {
	// unpack tar file
	tarPath := fmt.Sprintf("%s/go%s.linux-amd64.tar.gz", gi.downloadPath, tag.String())
	extractCmd := fmt.Sprintf("sudo tar -C %s -xzf %s", gi.extractDir, tarPath)
	cmd := exec.Command(values.Bash, "-c", extractCmd)

	_, err := cmd.Output()

	return err
}
