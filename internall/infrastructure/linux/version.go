package linux

import (
	"fmt"
	"gitlab.com/SzymonSkurski/go-auto-updater/internall/version"
	"os/exec"
	"strings"
)

type GoVersion struct {
	local *version.Tag
}

func NewGoVersion() *GoVersion {
	return &GoVersion{}
}

func (gv *GoVersion) GetLocalVersion() *version.Tag {
	gv.ResetLocalVersion()

	return gv.local
}

func (gv *GoVersion) ResetLocalVersion() {
	gv.local = version.New(0, 0, 0)

	app := "go"
	arg1 := "version"
	runCmd := fmt.Sprintf("%s %s ", app, arg1) // go version
	cmd := exec.Command("go", "version")
	stdout, err := cmd.Output()

	if err != nil {
		return
	}
	out := string(stdout)

	out, _ = strings.CutPrefix(out, runCmd)
	outs := strings.Split(out, " ")

	if len(outs) < 2 {
		return
	}

	v, _ := strings.CutPrefix(outs[0], "go")

	gv.local.SetFromString(v)
}

func (gv *GoVersion) IsLocalVersionOutdated(remote *version.Tag) bool {
	return remote.IsLatest(gv.GetLocalVersion())
}
