package values

const (
	LocalPath       = `/usr/local`
	GoPath          = `/usr/local/go`
	GoBckPath       = `/usr/local/go_bck`
	DownloadDirName = "Downloads"
	Bash            = `/bin/sh`
)
