package values

const (
	MItemUpgrade   = "upgrade"
	MItemDowngrade = "downgrade"
	MItemExit      = "exit"
	MItemBack      = "back"
	MItemOlder     = "older"
)
