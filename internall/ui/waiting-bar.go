package ui

import (
	"time"
)

type WaitingBar struct {
	printFunc  func()
	interval   time.Duration
	quitChanel chan bool
}

func NewWaitingBar(printFunc func(), interval time.Duration, quitChannel chan bool) *WaitingBar {
	return &WaitingBar{
		printFunc:  printFunc,
		interval:   interval,
		quitChanel: quitChannel,
	}
}

func (wb *WaitingBar) Run() {
	go wb.progress()
}

func (wb *WaitingBar) progress() {
	wb.printFunc()
	time.Sleep(wb.interval)
	select {
	case c := <-wb.quitChanel:
		{
			if c {
				return
			}
		}
	default:
		wb.progress()
	}
}
