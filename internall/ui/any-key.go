package ui

import (
	"bufio"
	"os"
)

func Input(r *Response) (string, error) {
	r.Display()

	b, err := bufio.NewReader(os.Stdin).ReadBytes('\n')

	return string(b), err
}

func PressAnyKey() {
	_, _ = Input(New().Info("press any key to continue ..."))
}
