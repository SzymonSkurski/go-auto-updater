package ui

import (
	"fmt"
	"os"
)

type Response struct {
	info  []string
	msg   []string
	warn  []string
	fatal []string
}

func New() *Response {
	return &Response{}
}

func (r *Response) Display() {
	// display info without color
	r.displayInfo()
	// display msg in green
	r.displayMessage()
	// display warn in yellow
	r.displayWarn()
	// display fatal in red
	r.displayFatal()
}

func (r *Response) Info(s string) *Response {
	r.info = append(r.info, s)

	return r
}

func (r *Response) Infof(f string, args ...any) *Response {
	r.info = append(r.info, fmt.Sprintf(f, args))

	return r
}

func (r *Response) Msg(s string) *Response {
	r.msg = append(r.msg, s)

	return r
}

func (r *Response) Msgf(f string, args ...any) *Response {
	r.msg = append(r.msg, fmt.Sprintf(f, args))

	return r
}

func (r *Response) Warn(s string) *Response {
	r.warn = append(r.warn, s)

	return r
}

func (r *Response) Warnf(f string, args ...any) *Response {
	r.warn = append(r.warn, fmt.Sprintf(f, args))

	return r
}

func (r *Response) Fatal(s string) *Response {
	r.fatal = append(r.fatal, s)

	return r
}

func (r *Response) Fatalf(f string, args ...any) *Response {
	r.fatal = append(r.fatal, fmt.Sprintf(f, args))

	return r
}

func (r *Response) displayInfo() {
	if len(r.info) < 1 {
		return
	}
	for _, l := range r.info {
		fmt.Println(SprintColor(l, NoColor))
	}
}

func (r *Response) displayMessage() {
	if len(r.msg) < 1 {
		return
	}
	for _, l := range r.msg {
		fmt.Println(SprintColor(l, GreenColor))
	}
}

func (r *Response) displayWarn() {
	if len(r.warn) < 1 {
		return
	}
	for _, l := range r.warn {
		fmt.Println(SprintColor(l, YellowColor))
	}
}

func (r *Response) displayFatal() {
	if len(r.fatal) < 1 {
		return
	}
	for _, l := range r.fatal {
		fmt.Println(SprintColor(l, RedColor))
	}
	os.Exit(1)
}
