package ui

import (
	"github.com/manifoldco/promptui"

	"gitlab.com/SzymonSkurski/go-auto-updater/internall/values"
)

func MainMenu() (string, error) {

	prompt := promptui.Select{
		Label: "Select Action",
		Items: []string{
			values.MItemUpgrade,
			values.MItemDowngrade,
			values.MItemExit,
		},
	}

	_, result, err := prompt.Run()

	return result, err
}
