package ui

import (
	"github.com/erikgeiser/promptkit/confirmation"
)

func Prompt(msg string) (bool, error) {
	conf := confirmation.New(msg, confirmation.NewValue(false))

	return conf.RunPrompt()
}
