package ui

import "fmt"

const (
	// NoColor reset color to terminaldefault
	NoColor     = "\033[0m"
	RedColor    = "\033[0;31m"
	YellowColor = "\033[0;33m"
	GreenColor  = "\033[0;32m"
	//CyanColor   = "\033[0;36m"
)

func SprintColor(s, c string) string {
	return fmt.Sprintf("%s%s%s", c, s, NoColor)
}
