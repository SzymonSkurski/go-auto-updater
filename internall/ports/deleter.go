package ports

type Deleter interface {
	DeleteLocal() error
}
