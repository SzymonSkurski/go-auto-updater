package ports

import "gitlab.com/SzymonSkurski/go-auto-updater/internall/version"

type RemoteVersioner interface {
	GetAllVersions() version.Tags
	GetLatestVersion() *version.Tag
	GetOlderVersions(tag *version.Tag) version.Tags
}
