package ports

import "gitlab.com/SzymonSkurski/go-auto-updater/internall/version"

type LocalVersioner interface {
	GetLocalVersion() *version.Tag
	IsLocalVersionOutdated(remote *version.Tag) bool
	ResetLocalVersion()
}
