package ports

type Backuper interface {
	// Store current local go version
	Store() error
	// Restore go from backup
	Restore() error
	Delete() error
}
