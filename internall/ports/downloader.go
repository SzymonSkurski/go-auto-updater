package ports

import "gitlab.com/SzymonSkurski/go-auto-updater/internall/version"

type Downloader interface {
	Download(v *version.Tag) error
}
