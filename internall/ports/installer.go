package ports

import "gitlab.com/SzymonSkurski/go-auto-updater/internall/version"

type Installer interface {
	Install(tag *version.Tag) error
}
