package main

import "gitlab.com/SzymonSkurski/go-auto-updater/cmd/app"

func main() {
	a := app.Init()
	a.Run()
}
