package app

import "gitlab.com/SzymonSkurski/go-auto-updater/internall/ports"

type App struct {
	Backup         ports.Backuper
	DeleteLocal    ports.Deleter
	DownloadRemote ports.Downloader
	Install        ports.Installer
	VersionLocal   ports.LocalVersioner
	VersionRemote  ports.RemoteVersioner
}

func NewApp(backup ports.Backuper,
	del ports.Deleter,
	dwn ports.Downloader,
	inst ports.Installer,
	ver ports.LocalVersioner,
	rVer ports.RemoteVersioner) *App {
	return &App{
		Backup:         backup,
		DeleteLocal:    del,
		DownloadRemote: dwn,
		Install:        inst,
		VersionLocal:   ver,
		VersionRemote:  rVer,
	}
}
