package app

import (
	"fmt"

	"gitlab.com/SzymonSkurski/go-auto-updater/internall/ui"
	"gitlab.com/SzymonSkurski/go-auto-updater/internall/version"
)

func (app *App) changeVersion(ver *version.Tag, r *ui.Response) {
	localV := app.VersionLocal.GetLocalVersion()

	// Prompt confirm
	pMsg := fmt.Sprintf("do u want change golang to go%s", ver)
	if ok, _ := ui.Prompt(pMsg); !ok {
		r.Warn("operation canceled")

		return
	}
	// Download expected go tar
	if err := app.DownloadRemote.Download(ver); err != nil {
		r.Fatal(err.Error())

		return
	}

	// Check if downloaded file is correct
	// Delete not correct file

	// Store current version in backup
	if !localV.IsEmpty() {
		ui.New().Info("create golang backup").Display()
		if err := app.Backup.Store(); err != nil {
			ui.New().Warnf("golang backup err: %s", err.Error()).Display()
		}

		ui.New().Info("delete current local go").Display()
		if err := app.DeleteLocal.DeleteLocal(); err != nil {
			ui.New().Fatalf("cannot delete local go: %s", err.Error()).Display()

			return
		}
	}

	// Install selected version
	if err := app.Install.Install(ver); err != nil {
		ui.New().Fatalf("cannot install new version: %s", err.Error()).Display()
		if !localV.IsEmpty() {
			// restoreBackupVersion
			ui.New().Info("restore backup version").Display()
			if err := app.Backup.Restore(); err != nil {
				ui.New().Warnf("restore from backup err: %s", err.Error()).Display()
			}
		}

		return
	}

	// delete redundant backup
	if !localV.IsEmpty() {
		ui.New().Info("delete go backup").Display()
		if err := app.Backup.Delete(); err != nil {
			ui.New().Warnf("cannot delete go backup: %s", err.Error()).Display()
		}
	}

	app.VersionLocal.ResetLocalVersion()
	ui.New().Msgf("current golang version: %s", app.VersionLocal.GetLocalVersion()).Display()
}
