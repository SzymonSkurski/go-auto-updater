package app

import (
	"fmt"
	"gitlab.com/SzymonSkurski/go-auto-updater/internall/infrastructure/httpR"
	"gitlab.com/SzymonSkurski/go-auto-updater/internall/infrastructure/linux"
	"gitlab.com/SzymonSkurski/go-auto-updater/internall/values"
	"runtime"
)

func Init() *App {
	// TODO config struct and possibility to take args
	// build app depends on os
	switch runtime.GOOS {
	case "linux":
		return linuxBuild()
	default:
		panic(fmt.Sprintf("unsupported os:%s", runtime.GOOS))
	}
}

func linuxBuild() *App {
	dPath := linux.NewDownloadPath("", values.DownloadDirName).GetDownloadPath()
	installer := linux.NewGoInstall(values.LocalPath, dPath)
	versioner := linux.NewGoVersion()

	return NewApp(linux.NewGoBackup(values.GoPath, values.GoBckPath),
		linux.NewGoDelete(values.GoPath),
		linux.NewGoDownload(dPath),
		installer,
		linux.NewGoVersion(),
		httpR.NewGoRemoteVersion(versioner.GetLocalVersion()))
}
