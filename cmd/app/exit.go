package app

import (
	"gitlab.com/SzymonSkurski/go-auto-updater/internall/ui"
)

func (app *App) Terminate() *ui.Response {
	r := ui.New()

	r.Fatal("aborting")

	return r
}
