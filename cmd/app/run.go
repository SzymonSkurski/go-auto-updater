package app

import (
	"fmt"

	"gitlab.com/SzymonSkurski/go-auto-updater/internall/ui"
	"gitlab.com/SzymonSkurski/go-auto-updater/internall/values"
)

func (app *App) Run() {
	for {
		ui.Banner(app.VersionLocal.GetLocalVersion().String())
		action := app.selectAction()
		app.runAction(action).Display()

		if action != values.MItemBack {
			ui.PressAnyKey() // time for read log
		}
	}
}

func (app *App) selectAction() string {

	r, err := ui.MainMenu()
	if err != nil {
		panic(err)
	}

	return r
}

func (app *App) runAction(action string) *ui.Response {
	r := ui.New()
	r.Warn(fmt.Sprintf("unknown option: %s", action))

	switch action {
	case values.MItemUpgrade:
		r = app.Upgrade()
	case values.MItemDowngrade:
		r = app.Downgrade(false)
	case values.MItemExit:
		r = app.Terminate()
	}

	return r
}
