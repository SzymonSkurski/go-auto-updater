package app

import (
	"github.com/manifoldco/promptui"

	"gitlab.com/SzymonSkurski/go-auto-updater/internall/ui"
	"gitlab.com/SzymonSkurski/go-auto-updater/internall/values"
	"gitlab.com/SzymonSkurski/go-auto-updater/internall/version"
)

// Downgrade go to selected version
func (app *App) Downgrade(older bool) *ui.Response {
	r := ui.New()

	// Get local go version
	localV := app.VersionLocal.GetLocalVersion()
	all := app.VersionRemote.GetAllVersions()
	if older && len(all) > 0 {
		all = app.VersionRemote.GetOlderVersions(all[len(all)-1])
	}

	// List all version (local go version as default)
	v, err := promptVersion(all.ToStrings("go v", ""), all.GetPosition(localV))
	if err != nil {
		r.Warnf("prompt version error: %s", err.Error())

		return r
	}
	if v == values.MItemBack {
		return r
	}

	if v == values.MItemOlder {
		return app.Downgrade(true)
	}

	downV := &version.Tag{}
	downV.SetFromString(v)

	// Change only if different from local one
	if downV.IsSame(localV) {
		ui.New().Warnf("\nselected version is same as current: %s", localV.String()).Display()

		return r
	}

	app.changeVersion(downV, r)

	return r
}

func promptVersion(versions []string, pos int) (string, error) {
	versions = append(versions, values.MItemOlder)
	versions = append(versions, values.MItemBack)

	prompt := promptui.Select{
		Label:     "Select Version",
		Items:     versions,
		CursorPos: pos,
	}

	_, result, err := prompt.Run()

	return result, err
}
