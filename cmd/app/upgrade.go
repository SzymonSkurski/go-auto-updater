package app

import (
	"gitlab.com/SzymonSkurski/go-auto-updater/internall/ui"
)

func (app *App) Upgrade() *ui.Response {
	r := ui.New()
	// upgrade go version
	// get current go version if any
	localV := app.VersionLocal.GetLocalVersion()

	ui.New().Msgf("local go version: %s", localV.String()).Display()
	ui.New().Info("looking for latest stable version").Display()
	// check latest stable version
	remoteV := app.VersionRemote.GetLatestVersion()

	ui.New().Msgf("remote latest go version: %s", remoteV).Display()

	// download only if higher than local one
	if !remoteV.IsLatest(localV) {
		ui.New().Warn("\nthere is no new golang version").Display()

		return r
	}
	app.changeVersion(remoteV, r)

	return r
}

//	func checkGoPath() bool {
//		path := os.Getenv("PATH")
//
//		return strings.Contains(path, `/usr/local/go/bin`)
//	}
//
//	func displayGoPathHint() {
//		msg := `Add /usr/local/go/bin to the PATH environment variable.
//		You can do this by adding the following line to your $HOME/.profile or /etc/profile (for a system-wide installation):
//
//		export PATH=$PATH:/usr/local/go/bin
//	   `
//
//		fmt.Println(msg)
//	}
