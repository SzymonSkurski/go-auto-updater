# Go Auto Updater
## General
Automatically update the golang version on your Linux.

Every time a new version of go comes out, we have to find out about it ourselves, download it, delete the old one and unpack and move the new one to bin. Now these steps will be automated.

The programme conducts the following operations:  
**upgrade** to latest:
 - checks the local version of go if it exists
 - checks what is the latest stable release of go
 - when there is a newer version it prompts whether to update
 - creates a download folder (default `~/Downloads`)
 - downloads the latest file `go<latest>.linux-amd64.tar.gz`
 - saves the old version of go as a backup
 - deletes the current (older) version of go
 - extracts the downloaded file and moves it to `/usr/bin`
 - deletes backup or restores if something goes wrong
 - checks that the path to go is in the PATH environment variable
 - displays what version of go is after changes  
**downgrade** to previous version:
 - checks the local version of go if it exists
 - checks what all currently available versions are
 - displays a list of go versions for selection
 - prompts whether to downgrade to selected version
- creates a download folder (default `~/Downloads`)
- downloads the latest file `go<latest>.linux-amd64.tar.gz`
- saves the old version of go as a backup
- deletes the current (older) version of go
- extracts the downloaded file and moves it to `/usr/bin`
- deletes backup or restores if something goes wrong
- checks that the path to go is in the PATH environment variable
- displays what version of go is after changes
## Getting started
### Build binary
  
```
go build -o go-updater ./cmd/main.go  && sudo chmod +x go-updater && sudo mv go-updater /usr/bin/go-updater
```

## Usage
now to update go version just run `go-updater`

### flags
**-df=\[download folder path\]**  
example: `-df='\home\skurskis\linux_downloads'` will set download path for \home\skurskis\linux_downloads
